---
abstract: In a perfect world, all of the data underlying your web app is accurate,
  complete, and error-free. But in the real world, we sometimes need to "fix" production
  data. This talk is about how to update live data as safely and quickly as possible.
duration: 25
level: Intermediate
presentation_url: https://www.slideshare.net/AaronKnight14/fixing-web-data-in-production/1
room: Madison
slot: 2017-10-07 10:45:00-04:00
speakers:
- Aaron Knight
title: 'Fixing web data in production: best practices for bad situations'
type: talk
video_url: https://youtu.be/ohk-1MIvpmg
---

In a perfect world, all of the data underlying your web app is accurate, complete, and error-free. But in the real world, production data sometimes needs to be updated. 

Perhaps you made some bad choices when setting up your models. Or maybe a bug introduced some inconsistent data. So how do you fix it? The naïve approach might be to run an update directly on your production database, but that kind of approach is extremely risky.

This talk will outline some best practices for fixing production data. We'll go beyond the obvious precautions such as testing your scripts and ensuring that you have a database backup. Instead, this talk will focus on writing scripts that are idempotent, reversible, and performant.