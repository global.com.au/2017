---
abstract: Emu86 is a browser-based interpreter for x86 assembly language written in
  Python, using the Django framework. It allows code-stepping, displays registers,
  flags, memory, and the stack, and provides online help for all instructions. It
  is a great learning tool for introducing students to assembler.
duration: 25
level: Intermediate
room: PennTop South
slot: 2017-10-07 15:10:00-04:00
speakers:
- Gene Callahan
title: 'Python for Education: Emulating x86 Assembler on the Web'
type: talk
video_url: https://youtu.be/rD89ZSyrWNA
---

Emu86 is a browser-based interpreter for x86 assembly language written in Python, using the Django framework. It allows code-stepping, displays registers, flags, memory, and the stack, and provides online help for all instructions. It is a great learning tool for introducing students to assembler, and it illustrates the potential of "courseware as code" for education.