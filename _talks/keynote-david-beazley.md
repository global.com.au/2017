---
duration: 50
room: PennTop South
slot: 2017-10-07 09:10:00-04:00
speakers:
- David Beazley
title: The Other Async
type: keynote
video_url: https://youtu.be/ggMg8zHAmew
---