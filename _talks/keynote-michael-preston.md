---
duration: 40
room: PennTop South
slot: 2017-10-06 09:10:00-04:00
speakers:
- Michael Preston
title: Building a rigorous, inclusive, and sustainable CSforAll movement in NYC and
  beyond
type: keynote
video_url: https://youtu.be/RniDQ9wc--4
---