---
name: PyTennessee
tier: community
site_url: https://www.pytennessee.org/
logo: pytn.png
---
PyTennessee is a regional Python conference held every year in Nashville, TN. The 2018 conference
will run for 2 days (February 10 and 11, 2018), and will feature approximately 300 eager attendees,
keynote addresses, 3 speaking tracks, 1 tutorial track, a killer hallway track, and a Young Coders
event. Get your tickets now at <https://pytn2018.eventbrite.com/>!
