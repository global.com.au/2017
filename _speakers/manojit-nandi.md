---
name: Manojit Nandi
talks:
- Algorithmic fairness and algorithmic discrimination
---

I am a Big Data Developer at AppEagle where I design data architectures and algorithms for intelligent re-pricing. I have previously worked as a Data Scientist at STEALTHbits Technologies and Verizon Wireless. I was a fellow in the 2013 Data Science for Social Good fellowship where I became interested in the potential capabilities for using data science to solve problems with high societal impact.