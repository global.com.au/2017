---
name: Eric Baer
talks:
- 'The Evolution of API Design: from RPC to GraphQL'
---

Eric has been developing software for over ten years ranging from embedded systems in C++ to high traffic APIs in Java and SPAs in JavaScript. For the past five years, Eric has developed a deep specialization in JavaScript and the associated ecosystem. In his current role, Eric is a lead engineer at Formidable in Seattle where he is driving large projects, and writing software around Babel, GraphQL, and i18n.

In 2017 I’ve spoken at VoxxedDays Bristol, VueConf and will be speaking at ChainReact in July. Additionally, I’ve spoken at Meetups in Seattle and Bangalore, presented at various corporate trainings and a number of internal events.
 
Sample Writing
  * https://medium.com/@baerbaerbaer
Sample Speaking
  * https://youtu.be/G3zavrK6iKA?list=PLWtmwtslenX1eXnY-bqjfko21pdZqXdEm
 
Thank you so much for your time and consideration, I’m looking forward to hearing back.