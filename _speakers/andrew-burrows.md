---
name: Andrew Burrows
talks:
- Keep On Mockin' In The Real World
---

A lifelong coder, I spent my teens doing BASIC and assembler, my twenties doing C++ and Java but my thirties have been devoted to Python. I’m a keen advocate of writing less code and more tests and I love how easy Python makes that. I’ve spent the last 10 years at Man AHL, a systematic Hedge Fund based in London. Currently I head a small but fantastic team developing the next generation of platform and tools for this technology-lead business.