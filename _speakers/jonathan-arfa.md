---
name: Jonathan Arfa
talks:
- 'Machine Music: Exploring Machine Learning By Generating Music Or How To Teach A
  Computer To Rock Out'
---

Jonathan Arfa is a Texan expatriate living in New York City. He likes food, Machine learning, and spending time outdoors.

He has an M.S. in Statistics from UCLA and a B.A. in Economics from Washington U. in St. Louis. He has worked in the energy industry and at tech firms including Magnetic and Facebook.