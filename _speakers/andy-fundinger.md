---
name: Andy Fundinger
talks:
- The 8 things that the happen at the "." between an object and the attribute
---

I am a Python developer and speaker with 13 years of Python experience, I have spoken twice at PyGotham as well as at other conferences.