---
name: Joe Cabrera
talks:
- 'Indexing all the things: Building your search engine in Python'
---

Joe Cabrera is a software engineer with 5 years of professional experience building scalable and distributed systems for companies including Relevvant, Vimeo and Jopwell. He has spoken at professional conferences and is involved in the Python open source community.

Joe is currently building scalable search and backend systems for Jopwell, Inc in New York, a diversity hiring platform.