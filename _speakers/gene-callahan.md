---
name: Gene Callahan
talks:
- 'Python for Education: Emulating x86 Assembler on the Web'
---

Gene Callahan teaches computer science at the NYU Tandon School of Engineering. He has over 20 years of software development experience, and has published numerous articles and papers in both trade magazines and academic journals.