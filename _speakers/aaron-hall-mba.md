---
name: Aaron Hall, MBA
talks:
- 'Valuing Job Offers - Finance 101 with Python '
---

Former financial advisor and business professor, currently teaching Python at world-class universities and working in finance.