---
name: Nicole Zuckerman
talks:
- Fixing the Tech Interview
---

Nicole attended Hackbright Academy in 2012, and has been in love with python ever since. She can currently be found at Clover Health, tackling with glee data pipelines and APIs and everything in between. In her free time, Nicole is an avid dancer and teacher, sci-fi book fanatic, soul and jazz aficionado, and cheese lover. She has an MA in English Literature and Women’s Studies from the University of Liverpool.
