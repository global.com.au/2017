---
name: Zach Marine
talks:
- 'Managing Complexity: Config As Code, Custom Ansible Modules, and You'
---

Zach Marine is a software engineer at Squarespace. As a member of the Data Engineering team, Zach's goal is to empower the success of the greater Squarespace org, and of the Analytics team specifically, by building pipelines and analyst-facing tools that make data easier to access and make analysis easier, more powerful, and more impactful.