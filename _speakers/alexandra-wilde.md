---
name: Alexandra Wilde
talks:
- Fighting Human Trafficking with Code
---

Hi! I'm a data engineer for the Human Trafficking Response Unit at the Manhattan DA's Office. I first learned Python in 2012 so that I could design my own logic puzzles after a brief obsession with SET card games, and now use it to build internal web applications and streamlined analytic tools for use in anti-trafficking investigations. I like finding unique ways to use data and software engineering to help others.